FROM python:3.8

WORKDIR /app/
RUN pip install --no-cache -U poetry

COPY pyproject.toml poetry.lock /app/
RUN python3.8 -m venv /app/venv/ \
    && . /app/venv/bin/activate \
    && poetry install --no-dev --no-root --no-interaction

ENTRYPOINT ["/app/venv/bin/jupyter", "nbconvert", "--execute", "--clear-output"]
