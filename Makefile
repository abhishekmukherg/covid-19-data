IMAGE := registry.gitlab.com/abhishekmukherg/covid-19-data
build::
	docker build -t "$(IMAGE)" .
GsAnalysis.ipynb:: build
	docker run --rm -v "$(PWD)/:/data/" --user "$(shell id -u):$(shell id -g)" "$(IMAGE)" "/data/GsAnalysis.ipynb"
